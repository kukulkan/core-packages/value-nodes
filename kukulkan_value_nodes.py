from kukulkan.model.node import Node
import kukulkan.attributes as _attributes


class Integer(Node):

    attributes = {
        'output': {
            'class': _attributes.Integer,
            'default_value': 0,
            'plug_type': 'output'
        }
    }


class Float(Node):

    attributes = {
        'output': {
            'class': _attributes.Float,
            'default_value': 0.0,
            'plug_type': 'output'
        }
    }


class Boolean(Node):

    attributes = {
        'output': {
            'class': _attributes.Boolean,
            'default_value': False,
            'plug_type': 'output'
        }
    }


class String(Node):

    attributes = {
        'output': {
            'class': _attributes.String,
            'default_value': '',
            'plug_type': 'output'
        }
    }
