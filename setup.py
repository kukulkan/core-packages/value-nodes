from setuptools import setup, find_packages


setup(
    name='value-nodes',
    version='0.1.0',
    description='Core typed nodes for Kukulkan.',
    author='',
    author_email='',
    license='MIT',
    py_modules=['kukulkan_value_nodes'],
    entry_points={
        'kukulkan.nodes': [
            'Integer=kukulkan_value_nodes:Integer',
            'Float=kukulkan_value_nodes:Float',
            'Boolean=kukulkan_value_nodes:Boolean',
            'String=kukulkan_value_nodes:String',
        ]
    },
    url='https://gitlab.com/kukulkan/core-packages/value-nodes',
    download_url='git+https://gitlab.com/kukulkan/core-packages/value-nodes.git#egg=value-nodes',
    install_requires=['kukulkan', 'attribute-types'],
    dependency_links=[
        'git+https://gitlab.com/kukulkan/kukulkan#egg=kukulkan',
        'git+https://gitlab.com/kukulkan/core-packages/attribute-types#egg=attribute-types',
    ]
)
